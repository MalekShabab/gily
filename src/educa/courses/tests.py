from django.test import TestCase
from courses.models import Subject, Course
from django.contrib.auth.models import User


class SubjectTestCase(TestCase):
    def setUp(self):
        Subject.objects.create(title="Programming", slug="programming")
        Subject.objects.create(title="Mathematics", slug="mathematics")
        Subject.objects.create(title="Physics", slug="physics")

    def test_subject__str__(self):
        programming = Subject.objects.get(title="Programming")
        assert programming.__str__() == 'Programming'


# Create your tests he0re.

class CouresTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(
            username='user', email='user@email.com', password='password1234')
        programming_subject = Subject.objects.create(title="Programming", slug="programming")
        Course.objects.create(title="Learn Python", slug="learn-python", owner=user, subject=programming_subject
                              , overview="Learn Python ")

    def test_course__str__(self):
        course = Course.objects.get(title="Learn Python")
        assert course.__str__() == 'Learn Python'

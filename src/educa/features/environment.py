from rest_framework.test import APIClient
from django.contrib.auth.models import User
from courses.models import Subject, Course


def before_every_scenario(context):
    context.user = User.objects.create_user(
        username='user', email='user@email.com', password='password1234')
    context.subject = Subject.objects.create(title="Programing", slug="programing")
    context.course = Course.objects.create(title="Learn Django", slug="learn-django", overview="Learn Django",
                                           subject=context.subject, owner=context.user)


    context.client = APIClient()
    context.client.force_authenticate(user=context.user)


def before_scenario(context, scenario):
    scenarios = [
        'Enroll Courses',
    ]

    if scenario.feature.name in scenarios:
        before_every_scenario(context)

from behave import *
from rest_framework import status


@given(u'I am on courses page')
def step_impl(context):
    pass


@when(u'I am enrolling a new courses (POST /courses/<id>/enroll/)')
def step_impl(context):
    url = f"/api/courses/{context.course.id}/enroll/"
    context.response = context.client.post(url)


@then(u'Courses should be enrolling')
def step_impl(context):
    assert context.response.status_code == status.HTTP_202_ACCEPTED
